<!doctype html>
<html>

    <head>
     <meta charset="utf-8">
     <title>Benzin Erweiterung</title>
     <link href="styleUebung1php.css" rel="stylesheet">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
     integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>

    <body> 
        <div class="styleAligen">
          <a href='index.php #uebung6'>Zurück zur Seite</a>
        </div>

        <section>
         <p class="styleAligen ueberschriftenStyle">Aufgabe 6 Benzin Erweiterung</p>
            <div class = "styleAligen">
                <?php
                  $rabattKarte = $_POST["rabattKarte"];
                  $benzinzArt = $_POST["benzinArt"];
                  $normalBenzin = 1.35 ;
                  $superBenzin = 1.40 ;
                  $dieselBenzin = 1.1 ;
                  $liter = $_POST["liter"];
                  $resultDieselBenzinRabatt = ($liter * $dieselBenzin) ;
                  $resultNormalBenzinRabatt = ($liter * $normalBenzin) ;
                  $resultSuperBenzinRabatt = ($liter * $superBenzin);
                  $resultDieselBenzin = $liter * $dieselBenzin ;
                  $resultNormalBenzin = $liter * $normalBenzin ;
                  $resultSuperBenzin = $liter * $superBenzin ;
                  $rabattDieselBenzin =$resultDieselBenzin - ($resultDieselBenzinRabatt /100 *2);
                  $rabattSuperBenzin =$resultSuperBenzinRabatt - ($resultSuperBenzinRabatt /100 *2);
                  $rabattNormalBenzin =$resultNormalBenzinRabatt - ($resultNormalBenzinRabatt /100 *2);
                  $ersparnisDiesel = $resultDieselBenzin - $rabattDieselBenzin;
                  $ersparnisNormal = $resultNormalBenzin - $rabattNormalBenzin;
                  $ersparnisSuper = $resultSuperBenzin - $rabattSuperBenzin;
                  
                  if($benzinzArt=="Diesel" && $rabattKarte=="Nein"){
                    echo "Die Tankfüllung für" .$liter." Liter Diesel  kostet : " .$resultDieselBenzin ." <br>" ;
                  }else 
                  if($benzinzArt=="Diesel" && $rabattKarte=="Ja"){
                    echo "Die Tankfüllung für ".$liter." Liter Diesel kostet : " .$rabattDieselBenzin ." Euro mit 2 % Rabatt<br>" ;
                    echo "Sie sparen : ".$ersparnisDiesel." Euro<br>";
                  }else
                  if ($benzinzArt=="Super" && $rabattKarte=="Nein"){
                    echo "Die Tankfüllung für " .$liter." Liter Super kostet : " .$resultSuperBenzin ."<br>" ;
                  } else 
                  if ($benzinzArt=="Super" && $rabattKarte=="Ja"){
                    echo "Die Tankfüllung für " .$liter." Liter Super kostet : " .$rabattSuperBenzin ." Euro mit 2 % Rabatt<br>" ;
                    echo "Sie sparen : ".$ersparnisDiesel." Euro<br>";
                  } else
                  if ($benzinzArt=="Normal" && $rabattKarte=="Nein"){
                    echo "Die Tankfüllung für " .$liter." Liter Normal  kostet : " .$resultNormalBenzin ." Euro<br>" ;
                  } else
                  if ($benzinzArt=="Normal" && $rabattKarte=="Ja"){
                    echo "Die Tankfüllung für " .$liter." Liter Normal  kostet : " .$resultNormalBenzin ." Euro mit 2 % Rabatt<br>" ;
                    echo "Sie sparen : ".$ersparnisNormal." Euro<br>";
                  } else  
                  echo "Falsche Eingabe<br>";
               
                ?>
            </div>
        </section>

    </body>

</html>