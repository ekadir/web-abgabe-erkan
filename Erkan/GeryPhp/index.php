<!doctype html>
<html>
  <head>
   <meta charset="utf-8">
   <title>UebungPhp</title>
   <link href="styleUebung1php.css" rel="stylesheet">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  </head>
  
  <body class="background">
    <!--<nav class="navbar navbar-expand-lg  navbar-dark bg-dark">
     <a class="navbar-brand" href="#">ÜbungenPhp</a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
     </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
       <ul class="navbar-nav mr-auto text-center">
          <li class="nav-item">
           <a class="nav-link px-4" href='index.php #uebung1'>Übung1
           <br>
          </li>
          <li class="nav-item">
           <a class="nav-link px-4" href='index.php #uebung2'>Übung2
           <br>
          </li>
          <li class="nav-item">
           <a class="nav-link px-4" href='index.php #uebung3'>Übung3
           <br>
          </li>
          <li class="nav-item">
           <a class="nav-link px-4" href='index.php #uebung4'>Übung4
           <br>
          </li>
          <li class="nav-item">
           <a class="nav-link px-4" href='index.php #uebung5'>Übung5
           <br>
          </li>
          <li class="nav-item">
           <a class="nav-link px-4" href='index.php #uebung6'>Übung6
           <br>
          </li>
          <li class="nav-item">
           <a class="nav-link px-4" href='index.php #uebung7'>Übung7
           <br>
          </li>
          <li class="nav-item">
           <a class="nav-link px-4" href='index.php #uebung8'>Übung8
           <br>
          </li>
        </ul>
      </div>
    </nav>-->

    <section >
      <p class="styleAligen ueberschriftenStyle">Aufgabe 1 : Brutto Netto</p>
      <div class=" styleAufgabe1">
        <?php

          $artikel1 = 22.5;
          $artikel2 = 12.3;
          $artikel3 = 5.2;

          $bruttoSumme = $artikel1 + $artikel2 +$artikel3;
          $umsatzSteuer = $bruttoSumme/100*20;
          $nettoSumme = $bruttoSumme -$umsatzSteuer;
     
          echo "Artikel1 : ".$artikel1." Euro<br>";
          echo "Artikel2 : ".$artikel2." Euro<br>";
          echo "Artikel3 : ".$artikel3." Euro<br>";
          echo  "<br>";
         
          echo "Die BruttoSumme  : " .$bruttoSumme ." Euro<br>";
          echo "Die UmsatzSteuer : " .$umsatzSteuer." Euro<br>";
          echo "Die NettoSumme   : " .$nettoSumme. " Euro<br>";

        ?>
      </div>
    </section>

    <section id ="uebung2">
      <p class="styleAligen ueberschriftenStyle">Aufgabe 2 : Zum Quadrat rechenen</p>
      <div class="styleAufgabe2">
        <form  action="eingabe.php" method="post">
          <label>Gib eine Zahl ein die du zum Quadrat rechnen willst  !</label>
          <input name="number" type="number" class="inputstyleAufgabe2 styleAligen form-control col-2 ">
          <button type="submit" class="btn btn-primary">Rechnen</button>
        </form>
      </div>
    </section>

    <section id = "uebung3">
      <p class="styleAligen ueberschriftenStyle">Aufgabe 3 : Benzinpreis rechenen</p>
      <div class="styleAufgabe3">
        <form  action="benzin.php" method="post">
          <p>Wie viel Liter willst du tanken ? </p>
          <div class = "inputstyleAufgabe3">
            <select name ="liter" class=" col-3 form-control  " id="exampleFormControlSelect1">
              <option>10</option>
              <option>20</option>
              <option>50</option>
              <option>100</option>
              <option>150</option>
              <option>200</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Rechnen</button>
        </form>
      </div>
    </section>

    <section id = "uebung4">
      <p class="styleAligen ueberschriftenStyle">Aufgabe 4 : Benzinpreis rechnen und Art waehlen </p>
      <div class="styleAufgabe4">
        <form  action="benzinwaehlen.php" method="post">
          <p>Wie viel Liter willst du tanken ? </p>
          <div class = "inputstyleAufgabe3">
            <select name ="liter" class=" col-3 form-control  " id="exampleFormControlSelect1">
              <option>10</option>
              <option>20</option>
              <option>50</option>
              <option>100</option>
              <option>150</option>
              <option>200</option>
            </select>
          </div>
          <p>Welchen Benzin willst du tanken ?</p>
          <div class = "inputstyleAufgabe4">
            <select name ="benzinArt" class=" col-3 form-control  " id="exampleFormControlSelect1">
              <option>Diesel</option>
              <option>Super</option>
              <option>Normal</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Rechnen</button>
        </form>
       
      </div>
    </section>

    <section id ="uebung5">
      <p class="styleAligen ueberschriftenStyle">Aufgabe 5 : Benzinpreis rechnen und Art wahlen mit Rabatt ab 120 Liter</p>
      <div class="styleAufgabe5">
        <form  action="benzinrabatt.php" method="post">
          <p>Wie viel Liter willst du tanken ? </p>
          <div class = "inputstyleAufgabe5">
            <select name ="liter" class=" col-3 form-control  " id="exampleFormControlSelect1">
              <option>10</option>
              <option>20</option>
              <option>50</option>
              <option>100</option>
              <option>150</option>
              <option>200</option>
            </select>
          </div>
            <p>Welchen Benzin willst du tanken ?</p>
            <div class = "inputstyleAufgabe5">
            <select name ="benzinArt" class=" col-3 form-control  " id="exampleFormControlSelect1">
              <option>Super</option>
              <option>Normal</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Rechnen</button>
        </form>
       
      </div>
    </section>

    <section id = "uebung6">
     <p class="styleAligen ueberschriftenStyle">Aufgabe 6 : Benzin Erweiterung </p>
      <div class="styleAufgabe6">
        <form  action="benzinerweiterung.php" method="post">
          <p>Haben sie eine Rabatt Karte ? </p>
          <div class="inputstyleAufgabe6">
            <select name ="rabattKarte" class=" col-3 form-control  " id="exampleFormControlSelect1">
              <option>Ja</option>
              <option>Nein</option>
            </select>
          </div>
          <p>Wie viel Liter willst du tanken ? </p>
          <div class = "inputstyleAufgabe6">
            <select name ="liter" class=" col-3 form-control  " id="exampleFormControlSelect1">
              <option>10</option>
              <option>20</option>
              <option>50</option>
              <option>100</option>
              <option>150</option>
              <option>200</option>
            </select>
          </div>
          <p>Welchen Benzin willst du tanken ?</p>
          <div class = "inputstyleAufgabe6">
            <select name ="benzinArt" class=" col-3 form-control  " id="exampleFormControlSelect1">
              <option>Diesel</option>
              <option>Super</option>
              <option>Normal</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Rechnen</button>
        </form>
       
      </div>
    </section>
     
    <section id = "uebung7">
      <p class="styleAligen ueberschriftenStyle">Aufgabe 7 Schleifen Ausgabe</p>
      <div class ="styleAufgabe7">
        <div class = "styleAligen  ">
          <?php
           for ($i=13; $i <=29;$i= $i+4 ) {
           echo $i." ";
           }
          ?>
        </div>
        <div class = "styleAligen  ">
          <?php
           for ($i=2; $i>=0.5;$i= $i-0.5 ) {
           echo $i." ";
           }
          ?>
        </div>
        <div class = "styleAligen ">
          <?php
           for ($j=5; $j <=13;$j= $j+2 ) {
           echo $j."Z "; 
           }
          ?>
        </div>
        <div class = "styleAligen ">
          <?php
           for ($j=1000; $j <=6000;$j= $j+1000 ) {
           echo $j." ";
           }
         ?>
        </div>
        <div class = "styleAligen ">
          <?php
           for ($j=1; $j <=3; $j++ ) {
           echo"a"." ". "b";
           echo $j." ";
           }
          ?>
        </div>
        <div class = "styleAligen ">
          <?php
           for ($j=2; $j <=23;$j= $j+10 ) {
           echo ("c".$j." c".($j+1)." ");
           }
          ?>
        </div>
        <div class = "styleAligen ">
          <?php
           for ($j=13; $j <=45;$j= $j+4 ) {
             if ($j==25||$j==29){
             }else{
             echo($j." ");
             }
           }
          ?>
        </div>
        <div class = "styleAligen styleAufgabeEinMalEins ">
          
          <?php
           for ($j=1; $j <=10;$j= $j++ ) {
             echo"<br>";
             for($j=1;$j<=10;$j++){
               echo($j." x ".$j." = ".$j*$j);
               echo"<br>"; 
              }
              }
          ?>
        </div>
      </div>                         
    </section> 
    
    <section id = "uebung8">
      <p class="styleAligen ueberschriftenStyle">Aufgabe 8 : Würfel Spiel </p>
      <div class = "styleAufgabe8">
        <?php
         $PlayerOneBegin = rand(1,2);
         print_r( "Test für PlayerBegin ".$PlayerOneBegin);
         echo "<br>";
         $PlayerTwoBegin ;
         $result_Player1 = 0;
         $result_Player2 = 0;
         print_r("PlayerOne = 0 ");
         echo "<br>";
         print_r(" PlayerTwo = 0 " );
         echo "<br>";
         // if($PlayerOneBegin == 1){
           while($result_Player1 <= 25 && $result_Player2 < 25) {
             $result_Player1 = ($Player1= rand(1,6)) + $result_Player1;
             print_r("PlayerOne gewürfelt = " . $Player1); 
             echo "<br>";
             $result_Player2 =( $Player2= rand(1,6)) + $result_Player2;
             print_r("PlayerTwo  gewürfelt = " . $Player2); 
             echo "<br>";   
             if($result_Player1 > 25 && $result_Player2 < $result_Player1) {
               echo"PlayerOne hat gewonnen mit " . $result_Player1 . " Punkten";
               echo "<br>";
              }else if ($result_Player2 > 25 && $result_Player1 < $result_Player2) {
                echo"PlayerTwo hat gewonnen mit " . $result_Player2 . " Punkten";
                echo "<br>";   
              }else {
                echo"Gesamt : PlayerOne " . $result_Player1 . " Punkte / PlayerTwo " . $result_Player2 . " Punkte";
                echo "<br>";
              }
              }
             /* }else{
              while($result_Player1 <= 25 && $result_Player2 < 25) {
              $result_Player2 = ($Player2= rand(1,6)) + $result_Player2;
              print_r("PlayerOne gewürfelt = " . $Player2); 
              echo "<br>";
              $result_Player1 =( $Player1= rand(1,6)) + $result_Player1;
              print_r("PlayerTwo  gewürfelt = " . $Player1); 
              echo "<br>";   
              if($result_Player1 > 25 && $result_Player2 < $result_Player1) {
                echo"PlayerOne hat gewonnen mit " . $result_Player1 . " Punkten";
                echo "<br>";
               }else if ($result_Player2 > 25 && $result_Player1 < $result_Player2) {
                 echo"PlayerTwo hat gewonnen mit " . $result_Player2 . " Punkten";
                 echo "<br>";   
               }else {
                 echo"Gesamt : PlayerOne " . $result_Player2 . " Punkte / PlayerTwo " . $result_Player1 . " Punkte";
                 echo "<br>";
               }
             }*/
        ?>
      </div>  
    </section>
  </body>
</html>