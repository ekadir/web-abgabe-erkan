<!doctype html>
<html>

    <head>
     <meta charset="utf-8">
     <title>Benzin wählen</title>
     <link href="styleUebung1php.css" rel="stylesheet">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
     integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>

    <body> 
        <div class="styleAligen">
          <a href='index.php #uebung4'>Zurück zur Seite</a>
        </div>

        <section>
         <p class="styleAligen ueberschriftenStyle">Aufgabe 4 Benzinpreis und Art wählen</p>
            <div class = "styleAligen">
                <?php
                  $benzinzArt = $_POST["benzinArt"];
                  $normalBenzin = 1.35 ;
                  $superBenzin = 1.40 ;
                  $dieselBenzin = 1.10 ;
                  $liter = $_POST["liter"];
                  $resultNormalBenzin = $liter * $normalBenzin ;
                  $resultSuperBenzin = $liter * $superBenzin ;
                  $resultDieselBenzin = $liter * $dieselBenzin ;
                   
                  if ($benzinzArt=="Diesel"){
                    echo "Die Tankfüllung für Diesel Benzin kostet : " .$resultDieselBenzin ." Euro<br>" ;
                  } else
                  if ($benzinzArt=="Normal"){
                    echo "Die Tankfüllung für Normal Benzin kostet : " .$resultNormalBenzin ." Euro<br>" ;
                  } else 
                  if ($benzinzArt=="Super"){   
                    echo "Die Tankfüllung für Super kostet : " .$resultSuperBenzin ." Euro<br>" ;
                  } else  {      
                    echo "Falsche Eingabe<br>";
                    echo "Gib Super für Super Benzin ein<br>";
                    echo "Gib Normal für Normal Benzin ein<br>";
                  } 
                  
                  
                ?>
            </div>
        </section>

    </body>

</html>